package myApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBconnect {

	private static DBconnect single;

	public Connection getConnection() {
		Connection con = null;

		if (con == null) {
			try {
				Class.forName("org.hsqldb.jdbcDriver");
				con = (Connection) DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/OneDB", "SA", "");
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return con;
	}

	public static DBconnect getInstance() {
		return (single == null) ? single = new DBconnect() : single;
	}

}
