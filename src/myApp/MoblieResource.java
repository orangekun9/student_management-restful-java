package myApp;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/moblies")
public class MoblieResource {

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
	public List<Moblie> getMoblie() {
		return MoblieDao.INSTANCE.getMoblies();
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
	@Path("{moblieNAME}")
	public List<Moblie> getMoblie(@PathParam("moblieNAME") String name) {
		return MoblieDao.INSTANCE.getMoblie(name);
	}

	@DELETE
	@Produces(MediaType.TEXT_HTML)
	@Path("/{moblieID}")
	public String deleteMoblieByID(@PathParam("moblieID") String id) {
		return MoblieDao.INSTANCE.deletebyid(Integer.parseInt(id));
	}

	@DELETE
	@Produces(MediaType.TEXT_HTML)
	public String deleteAllMoblie() {
		return MoblieDao.INSTANCE.deleteallMoblie();
	}

	@PUT
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/{moblieNAME}")
	public String putMoblie(@PathParam("moblieNAME") String currentname, @FormParam("name") String newname,
			@Context HttpServletResponse servletResponse) {
		return MoblieDao.INSTANCE.update(currentname, newname);
	}

	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String postMoblie(@FormParam("name") String name, @FormParam("price") String price,
			@FormParam("description") String description, @FormParam("author") String author,
			@Context HttpServletResponse httpServletResponse) {
		return MoblieDao.INSTANCE.postMoblie(name, price, description, author);
	}

	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/{moblieThree}")
	public String postThreeMoblie(@FormParam("name") String name, @FormParam("price") String price,
			@FormParam("description") String description, @FormParam("author") String author,
			@Context HttpServletResponse httpServletResponse) {
		return MoblieDao.INSTANCE.postThreeMoblie(name, price, description, author);
	}
}
