package myApp;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public enum MoblieDao {
	INSTANCE;

	private ArrayList<Moblie> mobliesList;

	private MoblieDao() {
	}

	public ArrayList<Moblie> getMoblies() {
		mobliesList = new ArrayList<Moblie>();
		try {
			Statement stmt = DBconnect.getInstance().getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM GETVIEW");
			while (rs.next()) {
				Moblie moblie = new Moblie();
				moblie.setId(rs.getInt(1));
				moblie.setName(rs.getString(2));
				moblie.setPrice(rs.getInt(3));
				moblie.setDescription(rs.getString(4));
				moblie.setAuthor(rs.getString(5));
				mobliesList.add(moblie);
			}
			stmt.close();
			return mobliesList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Moblie> getMoblie(String name) {
		try {
			mobliesList = new ArrayList<Moblie>();
			PreparedStatement prd = DBconnect.getInstance().getConnection()
					.prepareStatement("SELECT * FROM GETVIEW where name=?");
			prd.setString(1, name);
			ResultSet rs = prd.executeQuery();
			while (rs.next()) {
				Moblie moblie = new Moblie();
				moblie.setId(rs.getInt(1));
				moblie.setName(rs.getString(2));
				moblie.setPrice(rs.getInt(3));
				moblie.setDescription(rs.getString(4));
				moblie.setAuthor(rs.getString(5));
				mobliesList.add(moblie);
			}
			prd.close();
			return mobliesList;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unused")
	public String deletebyid(int id) {
		try {
			Moblie moblie = new Moblie();
			if (moblie == null)
				return "Error with delete data";
			else {
				Statement stmt = DBconnect.getInstance().getConnection().createStatement();
				return (stmt.executeUpdate("DELETE FROM MOBLIE where id = '" + id + "'") > 0 ? "Delete Successfully"
						: "Not Found Data");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Error with delete data";
	}

	public String deleteallMoblie() {
		try {
			Statement stmt = DBconnect.getInstance().getConnection().createStatement();
			stmt.executeUpdate("TRUNCATE TABLE MOBLIE RESTART IDENTITY AND COMMIT");
			stmt.executeUpdate("TRUNCATE TABLE OWNER RESTART IDENTITY AND COMMIT");
			return "Delete Successfully";
		} catch (SQLException e) {
			e.printStackTrace();
			return "Error with delete data";
		}
	}

	@SuppressWarnings("unused")
	public String update(String currentname, String newname) {
		try {
			Moblie moblie = new Moblie();
			if (moblie == null)
				return "Error with delete data";
			else {
				Statement stmt = DBconnect.getInstance().getConnection().createStatement();
				return (stmt.executeUpdate(
						"UPDATE MOBLIE SET name = '" + newname + "' where name= '" + currentname + "'") > 0
								? "Update Successfully"
								: "Not Found Data");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Error with Update data";
	}

	public String postMoblie(String name, String price, String description, String author) {
		try {
			CallableStatement cbstmt = DBconnect.getInstance().getConnection()
					.prepareCall("{call new_moblie(?,?,?,?)}");
			cbstmt.setString(1, name);
			cbstmt.setInt(2, Integer.parseInt(price));
			cbstmt.setString(3, description);
			cbstmt.setString(4, author);
			return (cbstmt.execute() ? "Error with Inserting Data" : "Insert Data Successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Error with Inserting Data";
	}

	public String postThreeMoblie(String name, String price, String description, String author) {
		try {
			PreparedStatement prdm = DBconnect.getInstance().getConnection()
					.prepareStatement("INSERT INTO MOBLIE (name, price,description) VALUES (?,?,?)");
			prdm.setString(1, name);
			prdm.setInt(2, Integer.parseInt(price));
			prdm.setString(3, description);

			PreparedStatement prda = DBconnect.getInstance().getConnection()
					.prepareStatement("INSERT INTO OWNER (author) VALUES (?)");
			prda.setString(1, author);
			prda.execute();
			
			return ( (prdm.execute() && prda.execute() )? "Error with Inserting Data" : "Insert Data Successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Error with Inserting Data";
	}

}
