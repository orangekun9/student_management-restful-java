package myApp;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParserException;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class MoblieClient {

	private JFrame frame;
	private Connection con;
	private JTable table;
	private DefaultTableModel model;
	private List<Moblie> moblietabledata;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MoblieClient window = new MoblieClient();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MoblieClient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 990, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 990, 26);
		frame.getContentPane().add(menuBar);

		JMenu mBar = new JMenu("Menu Bar");
		menuBar.add(mBar);

		JMenuItem CTable = new JMenuItem("Create Tables");
		mBar.add(CTable);

		JMenuItem FTable = new JMenuItem("Fill Table");
		mBar.add(FTable);

		JMenuItem PInfo = new JMenuItem("Project Info");
		mBar.add(PInfo);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(491, 37, 473, 317);
		frame.getContentPane().add(scrollPane);

		table = new JTable(new DefaultTableModel(new Object[][] {},
				new String[] { "Id", "Name", "Price", "Description", "Author" }));
		scrollPane.setViewportView(table);

		textField = new JTextField();
		textField.setBounds(164, 82, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Get");
		btnNewButton.setBounds(392, 81, 89, 23);
		frame.getContentPane().add(btnNewButton);

		JLabel lblNewLabel = new JLabel("Get Moblie By Name");
		lblNewLabel.setBounds(10, 85, 144, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblDeleteById = new JLabel("Delete By Id");
		lblDeleteById.setBounds(10, 137, 144, 14);
		frame.getContentPane().add(lblDeleteById);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(164, 134, 86, 20);
		frame.getContentPane().add(textField_1);

		JButton btnDelete = new JButton("Delete");

		btnDelete.setBounds(392, 133, 89, 23);
		frame.getContentPane().add(btnDelete);

		JLabel lblUpdateByName = new JLabel("Update By Name");
		lblUpdateByName.setBounds(10, 222, 133, 14);
		frame.getContentPane().add(lblUpdateByName);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(164, 219, 86, 20);
		frame.getContentPane().add(textField_2);

		JLabel lblCurrentName = new JLabel("Current name");
		lblCurrentName.setBounds(167, 183, 86, 14);
		frame.getContentPane().add(lblCurrentName);

		JLabel lblCurrentName_1 = new JLabel("New name");
		lblCurrentName_1.setBounds(263, 183, 86, 14);
		frame.getContentPane().add(lblCurrentName_1);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(263, 219, 86, 20);
		frame.getContentPane().add(textField_3);

		JButton btnPut = new JButton("Put");
		btnPut.setBounds(392, 218, 89, 23);
		frame.getContentPane().add(btnPut);

		JLabel lblPost = new JLabel("Post New Moblie");
		lblPost.setBounds(64, 289, 133, 14);
		frame.getContentPane().add(lblPost);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 336, 100, 14);
		frame.getContentPane().add(lblName);

		textField_4 = new JTextField();
		textField_4.setBounds(164, 333, 86, 20);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(10, 383, 100, 14);
		frame.getContentPane().add(lblPrice);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(164, 380, 86, 20);
		frame.getContentPane().add(textField_5);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(10, 436, 100, 14);
		frame.getContentPane().add(lblDescription);

		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(164, 433, 86, 20);
		frame.getContentPane().add(textField_6);

		JButton btnPost = new JButton("Post");
		btnPost.setBounds(392, 331, 89, 23);
		frame.getContentPane().add(btnPost);

		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 490, 100, 14);
		frame.getContentPane().add(lblAuthor);

		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(164, 487, 86, 20);
		frame.getContentPane().add(textField_7);

		JLabel lblNewLabel_1 = new JLabel("Single Get Area");
		lblNewLabel_1.setBounds(688, 418, 121, 14);
		frame.getContentPane().add(lblNewLabel_1);

		textArea = new JTextArea();
		textArea.setBounds(491, 443, 473, 33);
		frame.getContentPane().add(textArea);

		JButton btnClear = new JButton("Clear");
		btnClear.setBounds(491, 514, 89, 23);
		frame.getContentPane().add(btnClear);

		JButton btnExport = new JButton("Export");
		btnExport.setBounds(685, 514, 89, 23);
		frame.getContentPane().add(btnExport);

		JButton btnDe = new JButton("Delete All");
		btnDe.setBounds(853, 514, 89, 23);
		frame.getContentPane().add(btnDe);

		CTable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				init();
				if (con != null)
					JOptionPane.showMessageDialog(null, "Table Create Successfully", "Information",
							JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null, "Table Not Create Successfully", "Warning",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		FTable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				postThreeMoblie();
			}
		});

		PInfo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Student Number :- A00268807  \nName :- Vivek Modi", "Information",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField.getText().equals("")) {
					ClearAllField();
					JOptionPane.showMessageDialog(null, "Moblie by Name Field is Missng", "Warning",
							JOptionPane.WARNING_MESSAGE);
				} else
					getMoblieByName(textField.getText());
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField_1.getText().equals("")) {
					ClearAllField();
					JOptionPane.showMessageDialog(null, "Delete by Id Field is Missng", "Warning",
							JOptionPane.WARNING_MESSAGE);
				} else
					deleteById(Integer.parseInt(textField_1.getText()));
			}
		});

		btnPut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField_2.getText().equals("") || textField_3.getText().equals("")) {
					ClearAllField();
					JOptionPane.showMessageDialog(null, "Update By Name Field is Missng", "Warning",
							JOptionPane.WARNING_MESSAGE);
				} else
					updateByName(textField_2.getText(), textField_3.getText());
			}
		});

		btnPost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField_4.getText().equals("") || textField_5.getText().equals("")
						|| textField_6.getText().equals("") || textField_7.getText().equals("")) {
					ClearAllField();
					JOptionPane.showMessageDialog(null, "Post New Moblie Field is Missng", "Warning",
							JOptionPane.WARNING_MESSAGE);
				} else {
					postmoblie(textField_4.getText(), textField_5.getText(), textField_6.getText(),
							textField_7.getText());
				}
			}
		});

		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClearAllField();
			}
		});

		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filltables(true);
			}
		});

		btnDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deleteall();
			}
		});
	}

	private void init() {
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			con = (Connection) DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/OneDB", "SA", "");
			Statement stmt = con.createStatement();
			stmt.executeUpdate(
					"CREATE TABLE IF NOT EXISTS MOBLIE(id INTEGER IDENTITY PRIMARY KEY, name VARCHAR(32) NOT NULL, price INTEGER NOT NULL,description VARCHAR(32) NOT NULL)");
			stmt.executeUpdate(
					"CREATE TABLE IF NOT EXISTS OWNER(id INTEGER IDENTITY PRIMARY KEY,author VARCHAR(32) NOT NULL)");

			stmt.executeUpdate("DROP VIEW getview IF EXISTS");

			stmt.executeUpdate(
					"create view getview as select id,name,price,description,author from moblie join owner on moblie.id = owner.id");

			stmt.executeUpdate("DROP PROCEDURE new_moblie IF EXISTS");

			stmt.executeUpdate(
					"CREATE PROCEDURE new_moblie(IN name VARCHAR(50),IN price INTEGER,IN description VARCHAR(50), IN author VARCHAR(50)) MODIFIES SQL DATA "
							+ "BEGIN ATOMIC INSERT INTO MOBLIE VALUES (DEFAULT, name, price, description); INSERT INTO OWNER VALUES (DEFAULT, author);  END");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void postThreeMoblie() {
		String[] name = { "Oneplus", "Samsung", "Redmi" };
		String[] price = { "100", "200", "50" };
		String[] description = { "Android 10", "One UI 2.0", "Touch OS" };
		String[] author = { "BBK Electronics", "Samsung Group", "Xiaomi" };
		int count = 0;

		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies/moblieThree").build();
			System.out.println(uri.toString());

			HttpPost httpPost = new HttpPost(uri);
			httpPost.setHeader("Accept", "application/xml");
			CloseableHttpClient client = HttpClients.createDefault();

			for (int i = 0; i < name.length; i++) {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("name", name[i]));
				nameValuePairs.add(new BasicNameValuePair("price", price[i]));
				nameValuePairs.add(new BasicNameValuePair("description", description[i]));
				nameValuePairs.add(new BasicNameValuePair("author", author[i]));
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				System.out.println("Sending POST request...");
				CloseableHttpResponse response = client.execute(httpPost);
				String result = EntityUtils.toString(response.getEntity());
				if (result.equals("Insert Data Successfully")) {
					if (++count == name.length) {
						filltables(false);
						JOptionPane.showMessageDialog(null, result, "Information", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}

		} catch (URISyntaxException | IOException ex) {
			ex.printStackTrace();
		}
	}

	private void getMoblieByName(String name) {
		moblietabledata = new ArrayList<Moblie>();
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies/" + name + "").build();

			HttpGet httpReq = new HttpGet(uri);
			httpReq.setHeader("Accept", "application/xml");

			CloseableHttpResponse httpResponse = HttpClients.createDefault().execute(httpReq);

			System.out.println("Sending GET request...");
			ParseHelper parseBook = new ParseHelper();
			moblietabledata = parseBook.parse(EntityUtils.toString(httpResponse.getEntity()));

			if (moblietabledata.isEmpty()) {
				textArea.setText("");
				JOptionPane.showMessageDialog(null, "Data Not Found", "Warning", JOptionPane.WARNING_MESSAGE);
			} else {
				textArea.setText(moblietabledata.get(0).toString());
			}

		} catch (URISyntaxException | IOException | XmlPullParserException ex) {
			ex.printStackTrace();
		}
	}

	private void deleteById(int id) {
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies/" + id + "").build();

			HttpDelete httpDelete = new HttpDelete(uri);
			httpDelete.setHeader("Accept", "text/html");
			CloseableHttpClient client = HttpClients.createDefault();

			System.out.println("Sending DELETE request...");
			CloseableHttpResponse response = client.execute(httpDelete);
			String result = EntityUtils.toString(response.getEntity());
			filltables(false);
			JOptionPane.showMessageDialog(null, result, "Information", JOptionPane.INFORMATION_MESSAGE);
			textArea.setText("");
		} catch (URISyntaxException | IOException ex) {
			ex.printStackTrace();
		}
	}

	private void deleteall() {
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies").build();

			HttpDelete httpDelete = new HttpDelete(uri);
			httpDelete.setHeader("Accept", "text/html");
			CloseableHttpClient client = HttpClients.createDefault();

			System.out.println("Sending DELETE request...");
			CloseableHttpResponse response = client.execute(httpDelete);
			String result = EntityUtils.toString(response.getEntity());
			ClearAllField();
			filltables(false);
			JOptionPane.showMessageDialog(null, result, "Information", JOptionPane.INFORMATION_MESSAGE);
		} catch (URISyntaxException | IOException ex) {
			ex.printStackTrace();
		}
	}

	private void updateByName(String currentName, String newName) {
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies/" + currentName + "").build();

			HttpPut httpPut = new HttpPut(uri);
			httpPut.setHeader("Accept", "text/html");
			CloseableHttpClient client = HttpClients.createDefault();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("name", newName));
			httpPut.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			System.out.println("Sending PUT request...");
			CloseableHttpResponse response = client.execute(httpPut);
			filltables(false);
			JOptionPane.showMessageDialog(null, EntityUtils.toString(response.getEntity()), "Information",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (URISyntaxException | IOException ex) {
			ex.printStackTrace();
		}
	}

	private void postmoblie(String name, String price, String description, String author) {
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies").build();
			System.out.println(uri.toString());

			HttpPost httpPost = new HttpPost(uri);
			httpPost.setHeader("Accept", "application/xml");
			CloseableHttpClient client = HttpClients.createDefault();

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("name", name));
			nameValuePairs.add(new BasicNameValuePair("price", price));
			nameValuePairs.add(new BasicNameValuePair("description", description));
			nameValuePairs.add(new BasicNameValuePair("author", author));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			System.out.println("Sending POST request...");
			CloseableHttpResponse response = client.execute(httpPost);

			String result = EntityUtils.toString(response.getEntity());
			if (result.equals("Insert Data Successfully")) {
				filltables(false);
			}
			JOptionPane.showMessageDialog(null, result, "Information", JOptionPane.INFORMATION_MESSAGE);
		} catch (URISyntaxException | IOException ex) {
			ex.printStackTrace();
		}
	}

	private void filltables(boolean exportresponse) {
		moblietabledata = new ArrayList<Moblie>();
		try {
			URI uri = new URIBuilder().setScheme("http").setHost("localhost").setPort(8080)
					.setPath("/Distributed_Test_Project/myApp/moblies").build();

			HttpGet httpReq = new HttpGet(uri);
			httpReq.setHeader("Accept", "application/xml");

			CloseableHttpResponse httpResponse = HttpClients.createDefault().execute(httpReq);

			String result = EntityUtils.toString(httpResponse.getEntity());

			ParseHelper parseBook = new ParseHelper();
			model = (DefaultTableModel) table.getModel();
			model.setRowCount(0);
			moblietabledata = parseBook.parse(result);

			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("Export.csv")));
			bufferedWriter.write("Id" + "," + "Name" + "," + "Price" + "," + "Description" + "," + "Author\n");

			if (moblietabledata.isEmpty()) {
			} else {
				for (Moblie moblie : moblietabledata) {

					model.addRow(new Object[] { moblie.getId(), moblie.getName(), moblie.getPrice(),
							moblie.getDescription(), moblie.getAuthor() });

					if (exportresponse)
						bufferedWriter.write("\n" + moblie.getId() + "," + moblie.getName() + "," + moblie.getPrice()
								+ "," + moblie.getDescription() + "," + moblie.getAuthor());
				}
				bufferedWriter.close();
			}

			if (exportresponse)
				JOptionPane.showMessageDialog(null, "Import Successfully", "Information",
						JOptionPane.INFORMATION_MESSAGE);

		} catch (URISyntaxException | IOException | XmlPullParserException ex) {
			ex.printStackTrace();
		}
	}

	public void ClearAllField() {
		textArea.setText("");
		textField.setText("");
		textField_1.setText("");
		textField_2.setText("");
		textField_3.setText("");
		textField_4.setText("");
		textField_5.setText("");
		textField_6.setText("");
		textField_7.setText("");
	}
}
