package myApp;

import java.util.*;
import java.io.*;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class ParseHelper {

	private List<Moblie> moblieList;

	@SuppressWarnings("static-access")
	List<Moblie> parse(String response) throws XmlPullParserException, IOException {

		moblieList = new ArrayList<Moblie>();
		XmlPullParser pullParser = XmlPullParserFactory.newInstance().newPullParser();
		pullParser.setInput(new StringReader(response));

		Moblie tempmoblie = new Moblie();
		for (int event = pullParser.getEventType(); event != pullParser.END_DOCUMENT; event = pullParser.next()) {

			if (event == pullParser.START_TAG && pullParser.getName().equals("moblie")) {
				tempmoblie = new Moblie();
			}

			if (event == pullParser.START_TAG && pullParser.getName().equals("id")) {
				event = pullParser.next();
				tempmoblie.setId(Integer.parseInt(pullParser.getText()));
			}

			if (event == pullParser.START_TAG && pullParser.getName().equals("name")) {
				event = pullParser.next();
				tempmoblie.setName(pullParser.getText());
			}

			if (event == pullParser.START_TAG && pullParser.getName().equals("price")) {
				event = pullParser.next();
				tempmoblie.setPrice(Integer.parseInt(pullParser.getText()));
			}

			if (event == pullParser.START_TAG && pullParser.getName().equals("description")) {
				event = pullParser.next();
				tempmoblie.setDescription(pullParser.getText());
			}

			if (event == pullParser.START_TAG && pullParser.getName().equals("author")) {
				event = pullParser.next();
				tempmoblie.setAuthor(pullParser.getText());
			}

			if (event == pullParser.END_TAG && pullParser.getName().equals("moblie")) {
				moblieList.add(tempmoblie);
			}
		}
		return moblieList;
	}

}
